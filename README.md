# README #

### krb5_ccache.py ###

The krb5_ccache.py project is an attempt at creating a python Kerberos cache file parser.  I was learning about how to integrate a python app with a backend Kerberized service.  I wanted to really wrap my head around the TGT (Ticket-Granting-Ticket) and the Service Ticket data structures and there just wasn't much info out there.  I also wanted to be able to pull out the raw bytes of various keys/tokens.  The existing python tools were just wrappers around the C libraries.  This one is entirely in python. 

Thanks to [this page](http://www.gnu.org/software/shishi/manual/html_node/The-Credential-Cache-Binary-File-Format.html) for a complete description of the binary format

### How do I get set up? ###

* Just include the krb5_ccache.py file in your project and import it into your python module.

```
#!python

from krb5_ccache import Krb5_ccache

f = open('CredentialCacheFile', 'rb')
kc = Krb5_ccache(f)
print (kc)

```
 
There is a cool little feature I added for my testing that allows you to write out the raw bytes of any cache structure like so:


```
#!python

outFile.write(kc.credentials[0].getBytes())
```


### Contribution guidelines ###

* Any constructive development is appreciated. I'm not dictator.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact