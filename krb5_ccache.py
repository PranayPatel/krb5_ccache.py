# Original Author Pranay Patel
# www.pranaypatel.com

import struct
import binascii
import os
from datetime import datetime

class Item(object):
    def __init__(self, data):
        self.data = data
        self.bytesStart = 0
        self.bytesEnd = 0
        
    def parse(self, data):
        self.bytesStart = data.tell()
        self.doParse(data)
        self.bytesEnd = data.tell()

    def getBytes(self):
        lastPos = self.data.tell()
        self.data.seek(self.bytesStart)
        retVal = self.data.read(self.bytesStart-self.bytesEnd)
        self.data.seek(lastPos)
        return retVal

class Krb5_ccache(Item):
    def __init__(self, data):
        super(Krb5_ccache, self).__init__(data)
        self.fileFormatVersion = 0     # uint_16
        self.headerLen = 0             # uint_16   - only 0x0504
        self.headers = []              # header    - only 0x0504
        self.primaryPrincipal = None   # principal
        self.credentials = []          # credential
        self.parse(data)

    def doParse(self, data):       
        self.fileFormatVersion = struct.unpack('>H', data.read(2))[0]
        if(hex(self.fileFormatVersion) == "0x504"):
            self.headerLen = struct.unpack('>H', data.read(2))
            # get the headers
        else:
            self.primaryPrincipal = Principal(data)
            done = False
            while not done:
                self.credentials.append(Credential(data))
                done = data.tell() == os.fstat(data.fileno()).st_size

    def __str__(self):
        returnString = "---------------Primary Principal-----------------\n"
        returnString +=  "File Format Version: %s\n" % hex(self.fileFormatVersion)
        returnString += "Primary Principal: %s\n\n" % str(self.primaryPrincipal) 
        for credential in self.credentials:
            returnString += "---------------Credential-----------------"
            returnString += str(credential) + "\n"
        return returnString 

class Header(Item):
    def __init__(self, data):
        super(Header, self).__init__(data)

        self.tag = 0        # uint_16
        self.tagLen = 0     # uint_16
        self.tagData = []   # uint_8[tagLen]
        self.parse(data)

    def doParse(self, data):
        self.tag, self.tagLen = struct.unpack('>HH', data.read(4))
        self.tagData = struct.unpack(">%iB" % self.tagLen, data.read(self.tagLen))

    def __str__(self):
        return ( "Tag: %i" % self.tag + "\n" +
                 "TagData: %s" % self.tagData)

class DeltaTime(Item):
    def __init__(self, data):
        super(DeltaTime, self).__init__(data)

        self.timeOffset = 0     # uint_32
        self.usecOffset = 0     # uint_32
        self.parse(data)

    def doParse(self, data):
        self.timeOffset, self.usecOffset = struct.unpack('>II', data.read(8))

    def __str__(self):
        return ( "Time Offset: %i" % self.timeOffset + "\n" + 
                 "Usec Offset: %i" % self.usecOffset )

class Times(Item):
    def __init__(self, data):
        super(Times, self).__init__(data)

        self.authTime = 0   # uint_32
        self.startTime = 0  # uint_32
        self.endTime = 0    # uint_32
        self.renewTill = 0  # uint_32 
        self.parse(data)

    def doParse(self, data):
        self.authTime, self.startTime, self.endTime, self.renewTill = struct.unpack('>IIII', data.read(16))

    def getDateStr(self, value):
         return datetime.fromtimestamp(int(value)).strftime('%Y-%m-%d %H:%M:%S')

    def __str__(self):
        return ( "Auth Time: %s"  % self.getDateStr(self.authTime) + "\n" +
                 "Start Time: %s" % self.getDateStr(self.startTime) + "\n" +
                 "End Time: %s" % self.getDateStr(self.endTime) + "\n" +
                 "Renew Until: %s" % self.getDateStr(self.renewTill) + "\n")

class Address(Item):
    def __init__(self):
        super(Address, self).__init__(data)

        self.addrType = 0       # uint_16
        self.addrData = None    # counted_string
        self.parse(data)

    def doParse(self, data):
        self.addrType = struct.unpack('>H', data.read(2))
        self.addrData = CountedString(data)

    def __str__(self):
        return ( "Address Type: %i" % self.addrType + "\n" +
                 "Address %s" % str(self.addrData))

class AuthData(Item):
    def __init__(self):
        super(AuthData, self).__init__(data)

        self.authType = 0       # uint_16
        self.authData = None    # counted_string
        self.parse(data)

    def doParse(self, data):
        self.authType = struct.unpack('>H', data.read(2))
        self.authData = CountedString(data)
      
    def __str__(self):
        return ( "Auth Type: %i" % self.authType + "\n" +
                 "Auth Data %s" % str(self.authData))

class CountedString(Item):
    def __init__(self, data):
        super(CountedString, self).__init__(data)

        self.length = 0 # uint_32
        self.strdata = None  # uint_8[length]
        self.parse(data)

    def doParse(self, data):
        self.length = struct.unpack('>I', data.read(4))[0]
        self.strdata = ''.join([chr(x) for x in struct.unpack('>%iB' % self.length, data.read(self.length))])

    def __str__(self):
        return self.strdata
        

class Principal(Item):
    def __init__(self, data):
        super(Principal, self).__init__(data)

        self.nameType = 0           # uint_32
        self.numComponents = 0      # uitn_32
        self.realm = None           # counted_string
        self.components = []        # counted_string[numComponents]
        self.parse(data)

    def doParse(self, data):
        self.nameType, self.numComponents = struct.unpack('>II', data.read(8))
        self.realm = CountedString(data)
        for i in range(self.numComponents):
            self.components.append(CountedString(data))
       
    def __str__(self):
        return ("Name Type: %i" %  self.nameType + "\n" 
                "Realm: %s" % str(self.realm) + "\n"  +
                "Components: %s" % ', '.join([str(x) for x in self.components]))
                


class KeyBlock(Item):
    def __init__(self, data):
        super(KeyBlock, self).__init__(data)

        self.keyType = 0        # uint_16
        self.encType = 0        # uint_16
        self.keyLen = 0         # uint_16
        self.keyValue = 0       # uint_8[keyLen]
        self.parse(data)

    def doParse(self, data):
        self.keyType, self.encType, self.keyLen = struct.unpack('>HHI', data.read(8))
        self.keyValue = ''.join([chr(x) for x in struct.unpack('>%iB' % self.keyLen, data.read(self.keyLen))])

    def __str__(self):
        return ( "SKey Type: %s" % ENCTYPES.get(self.keyType, "Unknown") + "\n" + 
                 "TKT  Type: %s" % ENCTYPES.get(self.encType, "Unknown") + "\n" + 
                 "Key Length: %i" % self.keyLen + "\n" +
                 "Key Value: %s" % binascii.hexlify(self.keyValue) + "\n")

class Credential(Item):
    def __init__(self, data):
        super(Credential, self).__init__(data)

        self.client = None          # principal
        self.server = None          # principal
        self.key    = None          # keyblock
        self.time  = None           # times
        self.isSKey = 0             # uint8
        self.flags  = 0             # uint32 reversed byte order
        self.numAddress = 0         # uint32
        self.addresses = []         # address
        self.numAuthData = 0        # uint32
        self.authData = []          # authdata
        self.ticket = None          # counted_string
        self.secondTicket = None    # counted_string
        self.parse(data)

    def doParse(self, data):
        self.client = Principal(data)
        self.server = Principal(data)
        self.key = KeyBlock(data)
        self.time = Times(data)
        self.isSKey, self.flags, self.numAddress = struct.unpack('>BII', data.read(9))
        for i in range(self.numAddress): self.addresses.append(Address(data))
        self.numAuthData = struct.unpack('>I', data.read(4))[0]
        for t in range(self.numAuthData): self.authData.append(AuthData(data))
        self.ticket = CountedString(data)
        self.secondTicket = CountedString(data)

    def __str__(self):
        returnString =  "\n\nClient: %s\n" % self.client 
        returnString += "\nServer: %s\n" % self.server 
        returnString += "\n\n%s\n" % self.key 
        returnString += "%s\n" % self.time
        returnString += "Is SKey: %i\n" % self.isSKey
        returnString += "Flags: %s\n\n" % hex(self.flags)
        
        returnString += "Addresses (%i): \n" % self.numAddress
        for address in self.addresses:
            returnString += "%s\n" % address

        returnString += "AuthData (%i): \n" % self.numAuthData
        for aData in self.authData:
            returnSTring += "%s\n" % aData

        returnString += "Ticket: %s\n" % binascii.hexlify(str(self.ticket))
        returnString += "Second Ticket: %s\n" % self.secondTicket

        return returnString
                



ENCTYPES = {0:"reserved",
            1:"des-cbc-crc",
            2:"des-cbc-md4",
            3:"des-cbc-md5",
            4:"Reserved",
            5:"des3-cbc-md5",
            6:"Reserved",
            7:"des3-cbc-sha1",
            8:"Unassigned",
            9:"dsaWithSHA1-CmsOID",
            10:"md5WithRSAEncryption-CmsOID",
            11:"sha1WithRSAEncryption-CmsOID",
            12:"rc2CBC-EnvOID",
            13:"rsaEncryption-EnvOID",
            14:"rsaES-OAEP-ENV-OID",
            15:"des-ede3-cbc-Env-OID",
            16:"des3-cbc-sha1-kd",
            17:"aes128-cts-hmac-sha1-96",
            18:"aes256-cts-hmac-sha1-96",
            19:"Unassigned",
            20:"Unassigned",
            21:"Unassigned",
            22:"Unassigned",
            23:"rc4-hmac",
            24:"rc4-hmac-exp",
            25:"camellia128-cts-cmac",
            26:"camellia256-cts-cmac"}


#Permission to copy, modify, and distribute this document, with or
#without modification, for any purpose and without fee or royalty is
#hereby granted, provided that you include this copyright notice in ALL
#copies of the document or portions thereof, including modifications.

